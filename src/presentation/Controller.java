package presentation;

import java.awt.BorderLayout;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.fonts.*;
import com.itextpdf.text.FontFactory;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import Model.Produs;
import connection.ConnectionF;
import Model.Client;
import Model.Comanda;
import dataAccessLayer.ProdusDAO;
import dataAccessLayer.ClientDAO;
import dataAccessLayer.ComandaDAO;
import dataAccessLayer.Reflexie;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;

public class Controller implements ActionListener {

	View view;
	//public static final String REGULAR = "resources/OpenSans-Regular.ttf";
	public static final String BOLD = "resources/OpenSans-Bold.ttf";
	
	
	public Controller(View view) {
		this.view = view;
	}
	
	 public void actionPerformed(ActionEvent e) {
		 String command = e.getActionCommand();
         if(command.equals("addprod")) {
        	 view.addprodFrame.setVisible(true);
         }
         if(command.equals("okadd")) {
        	 String nume = view.numeProdText.getText();
        	 int pret = Integer.parseInt(view.pretProdText.getText());
        	 int cant = Integer.parseInt(view.cantProdText.getText());
        	 String stoc;
        	 if(cant>0)
        		 stoc="In stock";
        	 else stoc="Out of stock";
        	 Produs produs = new Produs(nume,cant,pret,stoc);
        	 ProdusDAO prodao = new ProdusDAO();
        	 int id = prodao.insert(produs);
        	 if(id>0) {
        		 System.out.println("Inserare cu success");
        	 }else System.out.println("Inserare fail");
        	 
        	 view.tp.removeAll();
        	 view.tp.addTab("Produse", view.createTable());
        	 view.tp.addTab("Clienti",view.clsTab);
        	 view.rowSorter = new TableRowSorter<TableModel>(view.prodTab.getModel());
        	 view.prodTab.setRowSorter(view.rowSorter);
        	 view.tp.revalidate();
        	 view.tp.repaint();
        	 view.adminFrame.revalidate();
        	 view.adminFrame.repaint();
        	 view.addprodFrame.setVisible(false);
         }
         if(command.equals("showord")){
        	 view.orderFrame.setVisible(true);
         }
         if(command.equals("edit_prod")) {  
        	 view.editProdFrame.setVisible(true);
         }
         if(command.equals("updateProd")) {  
        	 String stoc;
        	 String nume = view.numeUpText.getText();
        	 int cant = Integer.parseInt(view.cantUpText.getText());
        	 int pret = Integer.parseInt(view.pretUpText.getText());
        	 int id = Integer.parseInt(view.idUpText.getText());
        	 if(cant != 0)
        		 stoc = "In stock";
        	 else stoc = "Out of stock";
        	 Produs prod = new Produs(id,nume,cant,pret,stoc);
        	 ProdusDAO dao = new ProdusDAO();
        	 int x = dao.updateProd(prod);
        	 System.out.println("Update cu success");
        	// Connection dbconn = (Connection) ConnectionF.getConnection();
        	 view.tp.removeAll();
        	 
        	 view.tp.addTab("Produse", view.createTable());
        	 view.tp.addTab("Clienti",view.clsTab);
        	 view.rowSorter = new TableRowSorter<TableModel>(view.prodTab.getModel());
        	 view.prodTab.setRowSorter(view.rowSorter);
        	 view.tp.revalidate();
        	 view.tp.repaint();
        	 view.adminFrame.revalidate();
        	 view.adminFrame.repaint();
        	
        	 view.editProdFrame.setVisible(false);

        	
         }
         if(command.equals("login")) {  
        	String email = view.emailLogTxt.getText();
        //	String parola ="'"+ view.passLogTxt.getText()+"'";
        	String parola = new String(view.passLog.getPassword());
        	ClientDAO cd = new ClientDAO();
        	int permis = cd.findClient(email, parola);
        	System.out.println(permis);
        	if(permis == 0) // admmin
        	{
         		view.adminFrame.setVisible(true);
        	}else if(permis == 1) // client
        	{
        		view.clientFrame.setVisible(true);
        	}	
         }
         if(command.equals("creaza_log")) {  	
        	 view.createFrame.setVisible(true);
         }
         if(command.equals("okbuy")) {  	
        	 ComandaDAO cd = new ComandaDAO();
        	 String email = view.emailLogTxt.getText();
        	 int total = Integer.parseInt(view.total2.getText());
        	 int nr = view.nr_prod;
        	 String nume = cd.getNume("'"+email+"'");
        	 String adr = cd.getAdresa("'"+email+"'");
        	 String tel = cd.getTelefon("'"+email+"'");
        	 Comanda com = new Comanda(nume,email,total,nr,adr,tel);
        	 ProdusDAO p = new ProdusDAO();
        	 int id = cd.insert(com);
        	 for(int i =0;i<view.prodCump.size();i++) {
        		 p.decrementCant(view.prodCump.get(i));
        	 }
        	 createFactura(nume,adr,tel);
        	 view.infoBox("Factura generata cu succes", "Comanda");
        	 if(id>0) {
        		 System.out.println("Comanda plasata cu succes");
        	 }else System.out.println("Comanda esuata");
        	 // view.createFrame.setVisible(true);
         }
         if(command.equals("cumpara")) {  	
        	 view.buyFrame.setVisible(true);
         }
         if(command.equals("okset")) {  	
        	 String nume = view.numeSetText.getText();
        	 String adr = view.adrSetText.getText();
        	 String tel = view.telSetText.getText();
        	 String pas = view.parolaSetText.getText();
        	 String email = view.emailSetText.getText();
        	 String email2 = view.emailLogTxt.getText();
        	 ClientDAO cd = new ClientDAO();
        	 int id2 = cd.idClient(email2);
        	 int id = cd.updateClient(nume,adr,tel,pas,email, id2);
        	 System.out.println("Update client success");
        	 view.settFrame.setVisible(false);
         }
         if(command.equals("sett")) {  	
        	 view.settFrame.setVisible(true);
         }
         if(command.equals("delProd")) {  
        	 view.total2.setText("");
        	// view.prodFact.removeAll(null);
        	 view.produse.removeAll();
        	 view.buyFrame.repaint();
        	 view.buyFrame.revalidate();
        	// view.settFrame.setVisible(true);
         }
         if(command.equals("create_cont")) {  
        	 
        	 String nume = view.numeCreateTxt.getText();
        	 String adr = view.adresaCreateTxt.getText();
        	 String telefon = view.telefonCreateTxt.getText();
        	// String pas = view.parolaCreateTxt.getText();
        	 String pas = new String(view.passCr.getPassword());
        	 String email = view.emailContText.getText();
        	 ClientDAO cd = new ClientDAO();
        	 int r = cd.cautEmail(email);
        	 if(nume.isEmpty() || adr.isEmpty() || telefon.isEmpty() || email.isEmpty() || pas.isEmpty())
        	 {
        		 view.infoBox("Completati toate campurile", "Eroare");
        	 }else
        	 {
        		 if(telefon.length()==10){
        			 if(r<0)
        			 {
        				 if(view.group.getSelection().getActionCommand()=="client") {
        					 Client cl = new Client(nume,adr,telefon,pas,1,email);
        					 int id = cd.insertClient(cl);
        					 if(id>0) {
        						 System.out.println("Inserare cu success");
        					 }else System.out.println("Inserare fail");
        					 view.createFrame.setVisible(false);
        				 }
        				 if(view.group.getSelection().getActionCommand()=="admin") {
        					 Client cl = new Client(nume,adr,telefon,pas,0,email);
        					 int id = cd.insertClient(cl);
        					 if(id>0) {
        						 System.out.println("Inserare cu success");
        					 }else System.out.println("Inserare fail");
        					 view.createFrame.setVisible(false);
        				 }
        			 }else view.infoBox("Emailul exista deja", "Cont");
            	 }else View.infoBox("Eroare numar telefon", "Telefon");
        	 }	 
        }
	 }
	 	
	 /**
	  * Metoda creaza un document pdf in care scrie datele clientului ce a efectuat comanda si un tabel
	  * cu produsele cumparate, impreuna cu totalul de pret ale acestora
	  * 
	  * @param nume Numele clientului ce a efectuat comanda
	  * @param adr Adresa clientului ce a efectuat comanda
	  * @param tel Numarul de telefon al clientului ce a efectuat comanda
	  */
	 public void createFactura(String nume, String adr, String tel)
	 {
    	 try {
    		 Document doc = new Document();
    		 FontFactory.register(BOLD,"OpenSans-Bold");
    		 Font bold = FontFactory.getFont("OpenSans-Bold","Cp1253",true);
    		 PdfWriter.getInstance(doc, new FileOutputStream("Factura.pdf"));
    		 doc.open();
    		 Paragraph antet = new Paragraph("Factura\n",bold);
    		 Paragraph dateCL = new Paragraph(nume+"\n"+adr+"\n"+tel);
    		 antet.setAlignment(Element.ALIGN_RIGHT);
    		 doc.add(antet);
    		 doc.add(new Paragraph("Date Client\n",bold));
    		 doc.add(dateCL);
    		 doc.add(new Paragraph("\n"));
    		 doc.add(createFirstTable(view.prodFact,bold));
    	 	doc.close();
    	 }catch(DocumentException ex) {
    		 
    	 } catch (FileNotFoundException e1) { 
			e1.printStackTrace();
		}
	 }
	 /**
	  * Creaza un tabel cu produse ce urmeaza sa fie adaugat in pdf-ul cu factura
	  * @param p lista de produse cumparate
	  * @param bold fontul
	  * @return returneaza tabelul cu produse
	  */
	    public static PdfPTable createFirstTable(ArrayList<Produs> p,Font bold) {
	    	int sum=0;
	    	PdfPCell cell;
	        PdfPTable table = new PdfPTable(3);
	            table.addCell(createCell("Nume:", bold));
	            table.addCell(createCell("Qty:", bold));
	            table.addCell(createCell("Pret:", bold));
	        for(int i=0;i<p.size();i++) {
	        	table.addCell(createCell(p.get(i).getName()));
	        	table.addCell("1");
	        	table.addCell(createCell(String.valueOf(p.get(i).getPret())));
	        	sum+=p.get(i).getPret();
	        }
	        cell = new PdfPCell(new Phrase("Total: "+sum,bold));
	        cell.setColspan(3);
	        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        table.addCell(cell);
	        return table;
	    }
	    public static PdfPCell createCell(String text,Font font) {
	        return new PdfPCell(new Phrase(text,font));
	    }
	    public static PdfPCell createCell(String text) {
	        return new PdfPCell(new Phrase(text));
	    }
}
