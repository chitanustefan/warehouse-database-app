package presentation;

import java.awt.*;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import Model.Client;
import Model.Comanda;
import Model.Produs;
import dataAccessLayer.ClientDAO;
import dataAccessLayer.ComandaDAO;
import dataAccessLayer.ProdusDAO;
import dataAccessLayer.Reflexie;

public class View {

	public JFrame buyFrame,adminFrame,addprodFrame, orderFrame, loginFrame, createFrame, clientFrame, editProdFrame, settFrame;
	public JButton addProd, showOrder, editProd,loginBtn, createBtn, okUpdate;
	public JLabel numeProd, pretProd, cantProd, numeLogin,passLogin, numeCreate,adresaCreate,telefonCreate,parolaCreate,numeCom;
	public JLabel numeUpdate,pretUpdate,cantitateUpdate,idUpdate,total,total2, email, totalabel;
	public JTextField filterText,numeUpText, pretUpText,cantUpText,idUpText, emailContText,emailLogTxt,numeSetText,parolaSetText,adrSetText,telSetText,emailSetText;
	public JTextField numeProdText, pretProdText, cantProdText,numeLogTxt,passLogTxt, numeCreateTxt, adresaCreateTxt, telefonCreateTxt, parolaCreateTxt;
	public JPanel produse, numePaneProd, cantPaneProd, pretPaneProd,butoaneAdmin, loginNume,loginPass,butoaneClient;
	public JButton OKadd,btnCreate,addOrder,editCl,okBuy, okSet, deleteProd;
	public JRadioButton client, admin;
	public ButtonGroup group;
	public JTable prodTab, prodTab2,ordsTab,clsTab,myOrdsTab;
	public JScrollPane sp,sp2,sp3,spProd;
	public JLabel[] numeL;
	public int nr_prod = 0;
	public JLabel nume,parola,adresa,telefon,emailLogin,emailCreate;
	public ArrayList<Integer> prodCump = new ArrayList<Integer>();
	public ArrayList<Produs> prodFact = new ArrayList<Produs>();
	public JTabbedPane tp = new JTabbedPane();
	public  ProdusDAO pd = new ProdusDAO();
    public ClientDAO cl = new ClientDAO();
	public  ArrayList<Produs> prods;
    public ArrayList<Client> cls ;
    public TableRowSorter<TableModel> rowSorter = null;
    public JPasswordField passLog,passCr;
	
	
	public View() {
		
		GUI();
		addProd.setActionCommand("addprod");
		addProd.addActionListener(new Controller(this));
		OKadd.setActionCommand("okadd");
		OKadd.addActionListener(new Controller(this));
		showOrder.setActionCommand("showord");
		showOrder.addActionListener(new Controller(this));
		loginBtn.setActionCommand("login");
		loginBtn.addActionListener(new Controller(this));
		createBtn.setActionCommand("creaza_log");
		createBtn.addActionListener(new Controller(this));
		btnCreate.setActionCommand("create_cont");
		btnCreate.addActionListener(new Controller(this));
		client.setActionCommand("client");
		client.addActionListener(new Controller(this));
		admin.setActionCommand("admin");
		admin.addActionListener(new Controller(this));
		editProd.addActionListener(new Controller(this));
		editProd.setActionCommand("edit_prod");
		okUpdate.setActionCommand("updateProd");
		okUpdate.addActionListener(new Controller(this));
		addOrder.setActionCommand("cumpara");
		addOrder.addActionListener(new Controller(this));
		okBuy.setActionCommand("okbuy");
		okBuy.addActionListener(new Controller(this));
		editCl.setActionCommand("sett");
		editCl.addActionListener(new Controller(this));
		okSet.setActionCommand("okset");
		okSet.addActionListener(new Controller(this));


	}
	
	public void GUI() {
		
		nume = new JLabel("Nume: ");
		parola = new JLabel("Parola: ");
		adresa = new JLabel("Adresa: ");
		telefon = new JLabel("Telefon: ");
		
		//Frame creare cont
		createFrame = new JFrame("Creaza cont");
		createFrame.setLayout(new GridLayout(7,1));
	//	createFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		createFrame.setSize(400, 400);
		JPanel numeCr = new JPanel();
		JPanel adrCr = new JPanel();
		JPanel telCr = new JPanel();
		JPanel pas = new JPanel();
		JPanel btnCr = new JPanel();
		JPanel emailCr = new JPanel();
		numeCreate = new JLabel("Nume: ");
		adresaCreate = new JLabel("Adresa: ");
		telefonCreate = new JLabel("Telefon: ");
		parolaCreate = new JLabel("Parola: ");
		email = new JLabel("Email: ");
		emailCreate = new JLabel("Email: ");
		numeCreateTxt = new JTextField(20);
		adresaCreateTxt = new JTextField(20);
		telefonCreateTxt = new JTextField(20);
		//parolaCreateTxt = new JTextField(20);
		passCr = new JPasswordField(20);
		emailContText = new JTextField(20);
		btnCreate = new JButton("Creaza cont");
		btnCr.add(btnCreate);
		numeCr.add(numeCreate);
		numeCr.add(numeCreateTxt);
		
		emailCr.add(emailCreate);
		emailCr.add(emailContText);
		
		adrCr.add(adresaCreate);
		adrCr.add(adresaCreateTxt);
		
		telCr.add(telefonCreate);
		telCr.add(telefonCreateTxt);
		
		pas.add(parolaCreate);
		pas.add(passCr);
		
		createFrame.add(numeCr);
		createFrame.add(emailCr);
		createFrame.add(adrCr);
		createFrame.add(telCr);
		createFrame.add(pas);
		
		JPanel radioBut = new JPanel();
		client = new JRadioButton();
		client.setText("Client");
		admin = new JRadioButton();
		admin.setText("Admin");
		group = new ButtonGroup();
		group.add(client);
		group.add(admin);
		
		radioBut.add(client);
		radioBut.add(admin);
		createFrame.add(radioBut);
		createFrame.add(btnCreate);
		
		
		//Frame log in
		loginFrame = new JFrame("Log In");
		loginFrame.setLayout(new GridLayout(3,2));
		loginFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		loginFrame.setSize(300, 300);
	//	numeLogin = new JLabel("Nume: ");
		emailLogin = new JLabel("Email: ");
		passLogin = new JLabel("Pass: ");
		emailLogTxt = new JTextField(30);
		passLogTxt = new JTextField(30);
		passLog = new JPasswordField(30);
		loginBtn = new JButton("Login");
		createBtn = new JButton("Create");
		loginFrame.add(emailLogin);
		loginFrame.add(emailLogTxt);
		loginFrame.add(passLogin);
		//loginFrame.add(passLogTxt);
		loginFrame.add(passLog);
		loginFrame.add(loginBtn);
		loginFrame.add(createBtn);
		
		//Frame administrator
		adminFrame = new JFrame("Administrator");
		adminFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);;
		adminFrame.setSize(700, 500);
		adminFrame.setLayout(new BorderLayout());
		addProd = new JButton("Add prod");
		showOrder = new JButton("Show Orders");
		editProd = new JButton("Edit prod");
		butoaneAdmin = new JPanel();
		butoaneAdmin.add(addProd);
		butoaneAdmin.add(showOrder);
		butoaneAdmin.add(editProd);
		adminFrame.add(butoaneAdmin,BorderLayout.NORTH);
		
		//tabel produse
		JPanel cauta = new JPanel(new BorderLayout());
		cauta.add(new JLabel("Search"), BorderLayout.WEST);
		filterText = new JTextField(20);
		cauta.add(filterText, BorderLayout.CENTER);
		adminFrame.add(cauta, BorderLayout.SOUTH);
       
 /*       prods = pd.getAllProd();
        cls = cl.getAllClients();
		prodTab = Reflexie.createTable(prods);
		clsTab = Reflexie.createTable(cls);
		sp = new JScrollPane(prodTab); */
	    cls = cl.getAllClients();
		clsTab = Reflexie.createTable(cls);
		spProd = createTable();
		sp3 = new JScrollPane(clsTab);
		tp.addTab("Produse", sp);
		tp.addTab("Clienti", sp3);
		adminFrame.add(tp,BorderLayout.CENTER);
		
		rowSorter  = new TableRowSorter<TableModel>(prodTab.getModel());
		prodTab.setRowSorter(rowSorter);
		
		filterText.getDocument().addDocumentListener(new DocumentListener() {
			public void insertUpdate(DocumentEvent e) {
				String text = filterText.getText();
				if(text.trim().length()==0) {
					rowSorter.setRowFilter(null);
				}else {
					rowSorter.setRowFilter(RowFilter.regexFilter("(?i)"+text));
				}
			}
			public void removeUpdate(DocumentEvent arg0) {
				String text = filterText.getText();
                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
				
			}
			public void changedUpdate(DocumentEvent arg0) {
				throw new UnsupportedOperationException("Not supported yet.");
				
			}
		});
		
		addprodFrame = new JFrame("Adauga Produs");
	//	addprodFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addprodFrame.setSize(300,300);
		addprodFrame.setLayout(new GridLayout(4,1));
		numePaneProd = new JPanel();
		numePaneProd.setLayout(new FlowLayout());
		cantPaneProd  = new JPanel();
		cantPaneProd.setLayout(new FlowLayout());
		pretPaneProd = new JPanel();
		pretPaneProd.setLayout(new FlowLayout());
		numeProd = new JLabel("Nume: ", JLabel.LEFT);
		numeProdText = new JTextField(10);
		pretProd = new JLabel("Pret: ", JLabel.LEFT);
		pretProdText = new JTextField(10);
		cantProd = new JLabel("Cantitate: ", JLabel.LEFT);
		cantProdText = new JTextField(10);
		OKadd = new JButton("Adauga");
		
		numePaneProd.add(numeProd);
		numePaneProd.add(numeProdText);
		
		cantPaneProd.add(cantProd);
		cantPaneProd.add(cantProdText);
		
		pretPaneProd.add(pretProd);
		pretPaneProd.add(pretProdText);
		
		addprodFrame.add(numePaneProd);
		addprodFrame.add(cantPaneProd);
		addprodFrame.add(pretPaneProd);
		addprodFrame.add(OKadd);
		
		//Frame comenzi
		orderFrame = new JFrame("Comenzi");
	//	orderFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		orderFrame.setLayout(new BorderLayout());
		orderFrame.setSize(700,300);
        ComandaDAO cd = new ComandaDAO();
        ArrayList<Comanda> ords = cd.getAllOrders();
		ordsTab = Reflexie.createTable(ords);
		sp2 = new JScrollPane(ordsTab);
		orderFrame.add(sp2,BorderLayout.CENTER);

		
		//Frame edit prod
		editProdFrame = new JFrame("Editare produs");
	//	editProdFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		editProdFrame.setSize(300, 300);
		editProdFrame.setLayout(new GridLayout(5,1));
		okUpdate = new JButton("EDIT");
		JPanel numePed = new JPanel();
		JPanel idPed = new JPanel();
		JPanel cantPed = new JPanel();
		JPanel pretPed = new JPanel();
		numeUpdate = new JLabel("Nume: ");
		pretUpdate = new JLabel("Pret: ");
		cantitateUpdate = new JLabel("Cantitate: ");
		idUpdate = new JLabel("ID: ");
		numeUpText = new JTextField(15);
		pretUpText = new JTextField(6);
		cantUpText = new JTextField(4);
		idUpText = new JTextField(8);
		numePed.add(numeUpdate);
		numePed.add(numeUpText);
		idPed.add(idUpdate);
		idPed.add(idUpText);
		cantPed.add(cantitateUpdate);
		cantPed.add(cantUpText);
		pretPed.add(pretUpdate);
		pretPed.add(pretUpText);
		editProdFrame.add(idPed);
		editProdFrame.add(numePed);
		editProdFrame.add(cantPed);
		editProdFrame.add(pretPed);
		editProdFrame.add(okUpdate);
		
		//Frame client
		clientFrame = new JFrame("Client");
		clientFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		clientFrame.setSize(700, 700);
		clientFrame.setLayout(new BorderLayout());
		editCl = new JButton("Settings");
		addOrder = new JButton("Cumpara");
	//	deleteProd = new JButton("X");
		
		butoaneClient = new JPanel();
		butoaneClient.add(editCl);
		butoaneClient.add(addOrder);
	//	butoaneClient.add(deleteProd);
		clientFrame.add(butoaneClient,BorderLayout.NORTH);
        ProdusDAO pd1 = new ProdusDAO();
        ArrayList<Produs> prods1 = pd.getAllProd();
		prodTab2 = Reflexie.createTable(prods1);
		
		sp = new JScrollPane(prodTab2);
		clientFrame.add(sp,BorderLayout.CENTER);
		

		
		//Settings Frame
		settFrame = new JFrame("Settings");
		settFrame.setSize(300, 300);
		settFrame.setLayout(new GridLayout(6,2));
		JPanel numeSet = new JPanel();
		JPanel emailSet = new JPanel();
		JPanel adrSet = new JPanel();
		JPanel telSet = new JPanel();
		JPanel pasSet = new JPanel();
		numeSetText = new JTextField(20);
		parolaSetText = new JTextField(20);
		adrSetText = new JTextField(20);
		emailSetText = new JTextField(20);
		telSetText = new JTextField(20);
		numeSet.add(nume);
		numeSet.add(numeSetText);
		emailSet.add(email);
		emailSet.add(emailSetText);
		adrSet.add(adresa);
		adrSet.add(adrSetText);
		telSet.add(telefon);
		telSet.add(telSetText);
		pasSet.add(parola);
		pasSet.add(parolaSetText);
		okSet = new JButton("Modifica");
		settFrame.add(numeSet);
		settFrame.add(pasSet);
		settFrame.add(adrSet);		
		settFrame.add(emailSet);
		settFrame.add(telSet);
		settFrame.add(okSet);
		
		//Frame cumpara
		buyFrame = new JFrame("Cumpara");
		buyFrame.setSize(300, 300);
		buyFrame.setLayout(new BorderLayout());
		produse = new JPanel(new GridLayout(5,2));
		JPanel tot = new JPanel();
		JPanel down = new JPanel();
        final JLabel[] labels=createLabels();
        for (int i=0;i<10;i++){
        	produse.add(labels[i]);
        }
        okBuy= new JButton("Cumpara");
        total = new JLabel("Total ");
        total2 = new JLabel("");
        tot.add(total);
        tot.add(total2);
        down.add(okBuy);
        down.add(tot);
        buyFrame.add(produse, BorderLayout.CENTER);
		buyFrame.add(down,BorderLayout.SOUTH);
		

		
		
		prodTab2.addMouseListener(new java.awt.event.MouseAdapter() {
			int i =0;
			int tot=0;	
            public void mouseClicked(java.awt.event.MouseEvent evt) {
            	
            	DefaultTableModel model = (DefaultTableModel) prodTab2.getModel();
                int selectedRowIndex = prodTab2.getSelectedRow();
                numeCom = new JLabel();
                int id1 =  (Integer) model.getValueAt(selectedRowIndex, 0);
                String id = Integer.toString(id1);
                int cant1 = (Integer) model.getValueAt(selectedRowIndex, 2);
                String cant = Integer.toString(cant1);
                int pret1 = (Integer) model.getValueAt(selectedRowIndex, 3);
                String pret = Integer.toString(pret1);
                String nume = (String) model.getValueAt(selectedRowIndex, 1);
                if(cant1>0)
                {  	
                    labels[i].setText(nume);
                    labels[i+1].setText(pret);
                    i = i+2;
                    nr_prod++;
                    tot = tot + pret1;
                    prodCump.add(id1);
                    Produs p = new Produs(nume,pret1);
                    prodFact.add(p);
                    total2.setText(Integer.toString(tot));   
                }else infoBox("Produsul nu este disponibil","Eroare");
            
            }

        });
		
		prodTab.addMouseListener(new java.awt.event.MouseAdapter() { // row is clicked
            public void mouseClicked(java.awt.event.MouseEvent evt) {
            	DefaultTableModel model = (DefaultTableModel) prodTab.getModel();
                int selectedRowIndex = prodTab.getSelectedRow();
                int id1 =  (Integer) model.getValueAt(selectedRowIndex, 0);
                String id = Integer.toString(id1);
                int cant1 = (Integer) model.getValueAt(selectedRowIndex, 2);
                String cant = Integer.toString(cant1);
                int pret1 = (Integer) model.getValueAt(selectedRowIndex, 3);
                String pret = Integer.toString(pret1);
                
                
                idUpText.setText(id);
                numeUpText.setText((String) model.getValueAt(selectedRowIndex, 1));
                cantUpText.setText(cant);
                pretUpText.setText(pret);
            }
        });
		
		settFrame.setVisible(false);
		buyFrame.setVisible(false);
		clientFrame.setVisible(false);
		loginFrame.setVisible(true);
		createFrame.setVisible(false);
		orderFrame.setVisible(false);
		addprodFrame.setVisible(false);
		adminFrame.setVisible(false);
		editProdFrame.setVisible(false);
	//	myOrdFrame.setVisible(false);
	}
	
	public JScrollPane createTable() {
	    prods = pd.getAllProd();
		prodTab = Reflexie.createTable(prods);
		sp = new JScrollPane(prodTab);
		return sp;
	}
	
    private JPanel[] createPanels(){
    	JPanel[] panels=new JPanel[10];
        for (int i=0;i<10;i++){
            panels[i]=new JPanel();
        }
        return panels;
    }
	
    private JButton[] createButtons(){
        JButton[] btns=new JButton[10];
        for (int i=0;i<10;i++){
            btns[i]=new JButton("X");
        }
        return btns;
    }
	
	/**
	 * Metoda returneaza un vector de label-uri
	 * @return
	 */
    private JLabel[] createLabels(){
        JLabel[] labels=new JLabel[10];
        for (int i=0;i<10;i++){
            labels[i]=new JLabel("");
        }
        return labels;
    }
    /**
     * Metoda creaza un message dialog ce contine un mesaj informativ.
     * @param mesaj
     * @param titlu
     */
    public static void infoBox(String mesaj, String titlu)
    {
        JOptionPane.showMessageDialog(null, mesaj, titlu, JOptionPane.INFORMATION_MESSAGE);
    }
}
