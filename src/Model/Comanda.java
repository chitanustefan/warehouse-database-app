package Model;

public class Comanda {
	
	private String name;
	private int total;
	private int nr_prod;
	private String adresa;
	private String tel;
	private String email;
	private int id;
	
	public Comanda( int id, String name, String email, int total, int nr_prod, String adresa, String tel)
	{
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.total = total;
		this.nr_prod = nr_prod;
		this.adresa = adresa;
		this.tel = tel;
	}
	
	public Comanda( String name, String email, int total, int nr_prod, String adresa, String tel)
	{
		super();
		this.name = name;
		this.email = email;
		this.total = total;
		this.nr_prod = nr_prod;
		this.adresa = adresa;
		this.tel = tel;
	}

	public int getId() {
		return id;
	}
	public void setID(int id) {
		this.id=id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name=name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email=email;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total=total;
	}
	public int getNrProd() {
		return nr_prod;
	}
	public void setNr_prod(int nr_prod) {
		this.nr_prod=nr_prod;
	}
	public String getAdr() {
		return adresa;
	}
	public void setAdr(String adresa) {
		this.adresa=adresa;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel=tel;
	}
}
