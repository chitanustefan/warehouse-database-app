package Model;

public class Client {

	private int id;
	private String nume;
	private String adresa;
	private String telefon;
	private String parola;
	private int permis;
	private String email;
	
	public Client(int id, String nume, String adresa, String telefon, String parola, int permis, String email)
	{
		super();
		this.id = id;
		this.nume = nume;
		this.adresa = adresa;
		this.telefon = telefon;
		this.parola = parola;
		this.permis = permis;
		this.email = email;
	}
	public Client(String nume, String adresa, String telefon, String parola, int permis,  String email)
	{
		super();
		this.nume = nume;
		this.adresa = adresa;
		this.telefon = telefon;
		this.parola = parola;
		this.permis = permis;
		this.email = email;
	}
	
	public int getId() {
		return id;
	}
	public void setID(int id) {
		this.id=id;
	}
	public String getName() {
		return nume;
	}
	public void setName(String nume) {
		this.nume=nume;
	}
	public String getAdr() {
		return adresa;
	}
	public void setAdresa(String adresa) {
		this.adresa=adresa;
	}
	public String getTel() {
		return telefon;
	}
	public void setTel(String tel) {
		this.telefon=tel;
	}
	public String getPas() {
		return parola;
	}
	public void setPas(String parola) {
		this.parola=parola;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email=email;
	}
	public int getPermis() {
		return permis;
	}
	public void setPermis(int permis) {
		this.permis=permis;
	}
}
