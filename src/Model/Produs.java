package Model;

import java.util.ArrayList;

public class Produs {

	private int id;
	private String name;
	private int cantitate;
	private int pret;
	private String stoc;
	
	public Produs(int id, String name, int cantitate, int pret, String stoc)
	{
		super();
		this.id = id;
		this.name = name;
		this.cantitate = cantitate;
		this.pret = pret;
		this.stoc = stoc;
	}
	public Produs(String name, int cantitate, int pret, String stoc)
	{
		super();
		this.name = name;
		this.cantitate = cantitate;
		this.pret = pret;
		this.stoc = stoc;
	}
	public Produs(String name,int pret)
	{
		super();
		this.name = name;
		this.pret = pret;
	}

	
	public int getID() {
		return id;
	}
	public void setID(int id) {
		this.id=id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name=name;
	}
	public int getCant() {
		return cantitate;
	}
	public void setCant(int cantitate) {
		this.cantitate=cantitate;
	}
	public int getPret() {
		return pret;
	}
	public void setPret(int pret) {
		this.pret=pret;
	}
	public String getStoc() {
		return stoc;
	}
	public void setStoc(String stoc) {
		this.stoc=stoc;
	}
}
