package dataAccessLayer;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mysql.jdbc.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import Model.Client;
import Model.Produs;
import connection.ConnectionF;
import presentation.View;

public class ClientDAO {

	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insert = "INSERT INTO `baza`.`clienti`(`Nume`,`Adresa`,`Telefon`,`Parola`,`Permis`,`Email`)VALUES(?,?,?,?,?,?);";
	private static final String update = "UPDATE `baza`.`clienti` SET `Nume` = ?,`Adresa` = ?,`Telefon` = ?,`Parola` = ?,`Email` = ? WHERE Email = ? ;";
	private static final String sql = "SELECT `clienti`.`idclienti`,`clienti`.`Nume`,`clienti`.`Adresa`,`clienti`.`Telefon`,`clienti`.`Parola`,`clienti`.`Permis`,`clienti`.`Email`FROM `baza`.`clienti`;";
	//private static String find = "";
	public static String upNume = "";
	public static String upAdr = "";
	public static String upTel = "";
	public static String upPas = "";
	public static String upEmail = "";
	static View view;
	/**
	 * Metoda cauta in baza de date daca exista email-ul introdus de utilizator
	 * @param email
	 * @return
	 */
	public static int cautEmail(String email) {
		Connection dbconn = (Connection) ConnectionF.getConnection();
		PreparedStatement emailSt = null;
		String getEm="SELECT `clienti`.`Email` FROM  `baza`.`clienti` WHERE Email= '"+email+"' ;";
		ResultSet rs = null;
		int result=-1;
		try {
			emailSt = (PreparedStatement) dbconn.prepareStatement(getEm);
			rs = emailSt.executeQuery(getEm);
			if(rs.last())
			{
				result=1;
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	/**
	 * Metoda ce returneaza o lista cu toti clientii din baza de date
	 * @return
	 */
	public ArrayList<Client> getAllClients(){
		Connection dbconn = (Connection) ConnectionF.getConnection();
		ArrayList<Client> cls = new ArrayList<Client>();
		PreparedStatement findSt = null;
		ResultSet rs = null;
		try {
			findSt = (PreparedStatement) dbconn.prepareStatement(sql);
			rs = findSt.executeQuery(sql);
			while(rs.next())
			{
				if(rs.getInt("Permis")==1)
				{
					Client cl = new Client(rs.getInt("idclienti"), rs.getString("Nume"),rs.getString("Adresa"),rs.getString("Telefon"),rs.getString("Parola"),1,rs.getString("Email"));
					cls.add(cl);
				}
				
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return (cls);
	}
	
	public static int idClient(String email) {
		Connection dbconn = (Connection) ConnectionF.getConnection();
		PreparedStatement idSt = null;
		ResultSet rs = null;
		int id=0;
		String selID = "SELECT `clienti`.`idclienti` FROM `baza`.`clienti`  WHERE Email='"+email+"' ;";
		try {
			idSt = (PreparedStatement) dbconn.prepareStatement(selID);
			rs = idSt.executeQuery(selID);
			while(rs.next())
			{
				id = rs.getInt("idclienti");
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}
	
	/**
	 * Metoda ce updateaza datele unui client. Clientul este 'gasit' in functie de id
	 *
	 * @param nume Numele modificat al clientului
	 * @param adr Adresa modificata a clientului
	 * @param tel Telefonul modificat al clientului
	 * @param pas Parola modificata a clientului
	 * @param email Emailul modificat al clientului
	 * @param id Id-ul clientului
	 * @return
	 */
	public static int updateClient(String nume, String adr,String tel,String pas , String email, int id) {
		Connection dbconn = (Connection) ConnectionF.getConnection();
		PreparedStatement updateClient = null;
		int upId = -1;
		try {
				if(!nume.isEmpty()) {
					upNume = "UPDATE `baza`.`clienti` SET `Nume` = ? WHERE idclienti = ? ;";
					PreparedStatement numeSt = (PreparedStatement) dbconn.prepareStatement(upNume, Statement.RETURN_GENERATED_KEYS);
					numeSt.setString(1, nume);
					numeSt.setInt(2, id);
					numeSt.executeUpdate();
					ResultSet rs1 = numeSt.getGeneratedKeys();
				}
				if(!adr.isEmpty()) {
					upAdr = "UPDATE `baza`.`clienti` SET `Adresa` = ? WHERE idclienti =? ;";
					PreparedStatement adrSt = (PreparedStatement) dbconn.prepareStatement(upAdr,Statement.RETURN_GENERATED_KEYS);
					adrSt.setString(1, adr);
					adrSt.setInt(2, id);
					adrSt.executeUpdate();
					ResultSet rs2 = adrSt.getGeneratedKeys();

				}
				if(!tel.isEmpty()) {
					upTel = "UPDATE `baza`.`clienti` SET `Telefon` = ? WHERE idclienti =? ;";
					PreparedStatement telSt = (PreparedStatement) dbconn.prepareStatement(upTel,Statement.RETURN_GENERATED_KEYS);
					telSt.setString(1, tel);
					telSt.setInt(2, id);
					telSt.executeUpdate();
					ResultSet rs3 = telSt.getGeneratedKeys();

				}
				if(!pas.isEmpty()) {
					upPas = "UPDATE `baza`.`clienti` SET `Parola` = ? WHERE idclienti =? ;";
					PreparedStatement pasSt = (PreparedStatement) dbconn.prepareStatement(upPas,Statement.RETURN_GENERATED_KEYS);
					pasSt.setString(1, pas);
					pasSt.setInt(2, id);
					pasSt.executeUpdate();
					ResultSet rs4 = pasSt.getGeneratedKeys();

				}
				if(!email.isEmpty()) {
					upEmail = "UPDATE `baza`.`clienti` SET `Telefon` = ? WHERE idclienti =? ;";
					PreparedStatement emailSt = (PreparedStatement) dbconn.prepareStatement(upEmail,Statement.RETURN_GENERATED_KEYS);
					emailSt.setString(1, email);
					emailSt.setInt(2, id);
					emailSt.executeUpdate();
					ResultSet rs5 = emailSt.getGeneratedKeys();

				}
			}catch (Exception e) {
			e.printStackTrace();
		}
		return upId;
	}
	
	public static int findClient(String email, String parola) {
		String find = "SELECT * FROM `baza`.`clienti` WHERE Email = '"+email+"' and Parola ='"+parola+"' ;";
		Connection dbconn = (Connection) ConnectionF.getConnection();
		PreparedStatement findSt = null;
		ResultSet rs = null;
		int permis=-1;
		try {
			findSt = (PreparedStatement) dbconn.prepareStatement(find);
			rs = findSt.executeQuery(find);
			if(rs.next())
			{
				if(email.equals(rs.getString("Email")) && parola.equals(rs.getString("Parola")))
				{
					System.out.println(parola);
					permis = rs.getInt("Permis");
					
				}
					
			}else view.infoBox("Email sau parola invalide", "Eroare la conectare");	
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO: find " + e.getMessage());
		} finally {
			ConnectionF.close(rs);
			ConnectionF.close(findSt);
			ConnectionF.close(dbconn);
		}
		return permis;
		
	}
	/**
	 * Metoda insereaza un client in baza de date
	 * @param cl Client
	 * @return
	 */
	public static int insertClient(Client cl) {
		Connection dbcon = (Connection) ConnectionF.getConnection();
		PreparedStatement insertClient = null;
		int insertId =  -1;
		try {
			
			insertClient = (PreparedStatement) dbcon.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
			insertClient.setString(1, cl.getName());
			insertClient.setString(2, cl.getAdr());
			insertClient.setString(3, cl.getTel());
			insertClient.setString(4, cl.getPas());
			insertClient.setInt(5, cl.getPermis());
			insertClient.setString(6, cl.getEmail());
			insertClient.executeUpdate();
			
			ResultSet rs = insertClient.getGeneratedKeys();
			if(rs.next()) {
				insertId= rs.getInt(1);
			}
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO: insert", e.getMessage());
		} finally { ConnectionF.close(insertClient);
					ConnectionF.close(dbcon);
		}
		return insertId;
	}
	
}
