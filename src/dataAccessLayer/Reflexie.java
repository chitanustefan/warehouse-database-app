package dataAccessLayer;

import java.awt.Component;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import presentation.View;
public class Reflexie {

	static JTable tab;	
	
	/**
	 * Metoda returneaza un tabel de tip JTable ce este populat cu datele obiectelor din lista transmisa ca parametru
	 * @param objects Obiecte ce urmeaza sa fie inserate in tabel
	 * @return returneaaza un JTable
	 */
	public static <T> JTable createTable(List<T> objects) {
		ArrayList<String> collumnNamesArrayList = new ArrayList<String>(); 
		
		for(Field field : objects.get(0).getClass().getDeclaredFields()) {
			field.setAccessible(true);
			collumnNamesArrayList.add(field.getName());
		}
		
		String[] columnNames = new String[collumnNamesArrayList.size()];
		columnNames = collumnNamesArrayList.toArray(columnNames);
		DefaultTableModel tableModel = new DefaultTableModel(columnNames, 0);
		Iterator<T> i = objects.iterator();
		
		while(i.hasNext()) {
			T object = i.next();
			ArrayList<Object> columnDataAsArrayList = new ArrayList<Object>();
			for(Field field : object.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				try {
					columnDataAsArrayList.add(field.get(object));
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
			Object[] columnDataAsArray = new Object[columnDataAsArrayList.size()];
			columnDataAsArray = columnDataAsArrayList.toArray(columnDataAsArray);
			tableModel.addRow(columnDataAsArray);
		}
		tab = new JTable(tableModel);
		return tab;
	}
	
}
