package dataAccessLayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import Model.Comanda;
import Model.Produs;
import connection.ConnectionF;

public class ComandaDAO {
	
	protected static final Logger LOGGER = Logger.getLogger(ComandaDAO.class.getName());
	private static final String insertComanda = "INSERT INTO `baza`.`comenzi`(`client`,`email`,`total`,`nrprod`,`adresa`,`telefon`)VALUES(?,?,?,?,?,?);";
	private static String findAdr ="SELECT `clienti`.`Adresa` FROM `baza`.`clienti`WHERE Email =";
	private static String findTel = "SELECT `clienti`.`Telefon` FROM `baza`.`clienti`WHERE Email =";
	private static String findNume = "SELECT `clienti`.`Nume` FROM `baza`.`clienti`WHERE Email =";
	private static String sql ="SELECT `comenzi`.`idcomenzi`,`comenzi`.`client`, `comenzi`.`email`,`comenzi`.`total`,`comenzi`.`nrprod`, `comenzi`.`adresa`, `comenzi`.`telefon`FROM `baza`.`comenzi`;";  
	
	/**
	 * Metoda returneaza o lista cu toate comenzile plasate 
	 * @return
	 */
	public ArrayList<Comanda> getAllOrders(){
		Connection dbconn = (Connection) ConnectionF.getConnection();
		ArrayList<Comanda> ord = new ArrayList<Comanda>();
		PreparedStatement findSt = null;
		ResultSet rs = null;
		try {
			findSt = (PreparedStatement) dbconn.prepareStatement(sql);
			rs = findSt.executeQuery(sql);
			while(rs.next())
			{
				Comanda com = new Comanda(rs.getInt("idcomenzi"), rs.getString("client"),rs.getString("email"),rs.getInt("total"),rs.getInt("nrprod"),rs.getString("adresa"),rs.getString("telefon"));
				ord.add(com);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return (ord);
	}
	
	public static int insert(Comanda comanda) {
		Connection dbconnection = (Connection) ConnectionF.getConnection();
		PreparedStatement insertOrd = null;
		int insertId = -1;
		int stoc;
		try {
			
			insertOrd = (PreparedStatement) dbconnection.prepareStatement(insertComanda, Statement.RETURN_GENERATED_KEYS);
			insertOrd.setString(1, comanda.getName());
			insertOrd.setString(2, comanda.getEmail());
			insertOrd.setInt(3, comanda.getTotal());
			insertOrd.setInt(4, comanda.getNrProd());
			insertOrd.setString(5, comanda.getAdr());
			insertOrd.setString(6, comanda.getTel());
			insertOrd.executeUpdate();
			
			ResultSet rs = insertOrd.getGeneratedKeys();
			
			if(rs.next()) {

				insertId= rs.getInt(1);
			}
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "ComandaDAO: insert", e.getMessage());
		} finally { ConnectionF.close(insertOrd);
					ConnectionF.close(dbconnection);
		}
		return insertId;
	}
	/**
	 * Metoda returneaza adresa clientului care a plasat comanda
	 * @param email Emailul clientului care a plasat comanda
	 * @return
	 */
	public String getAdresa(String email){
		Connection dbconn = (Connection) ConnectionF.getConnection();
		String adr = "";
		PreparedStatement find = null;
		ResultSet rs = null;
		int i=0;
		try {
			findAdr = findAdr + email +" ;";
			find = (PreparedStatement) dbconn.prepareStatement(findAdr);
			rs = find.executeQuery(findAdr);
			while(rs.next())
			{
				i++;
				if(i<=1)
				{
					adr= rs.getString("Adresa");
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return adr;
	}
	
	public String getTelefon(String email){
		Connection dbconn = (Connection) ConnectionF.getConnection();
		String tel = "";
		PreparedStatement find = null;
		ResultSet rs = null;
		int i=0;
		try {
			findTel = findTel + email +" ;";
			find = (PreparedStatement) dbconn.prepareStatement(findTel);
			rs = find.executeQuery(findTel);
			while(rs.next())
			{
				i++;
				if(i<=1)
				{
					tel= rs.getString("Telefon");
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return tel;
	}
	
	public String getNume(String email){
		Connection dbconn = (Connection) ConnectionF.getConnection();
		String nume = "";
		PreparedStatement find = null;
		ResultSet rs = null;
		int i=0;
		try {
			findNume = findNume + email +" ;";
			find = (PreparedStatement) dbconn.prepareStatement(findNume);
			rs = find.executeQuery(findNume);
			while(rs.next())
			{
				i++;
				if(i<=1)
				{
					nume= rs.getString("Nume");
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return nume;
	}
	
}
