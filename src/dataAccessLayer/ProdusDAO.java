package dataAccessLayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mysql.jdbc.*;

import connection.ConnectionF;
import net.proteanit.sql.DbUtils;
import Model.Produs;
import presentation.View;

public class ProdusDAO {

	protected static final Logger LOGGER = Logger.getLogger(ProdusDAO.class.getName());
	private static final String insertProd = "INSERT INTO `baza`.`produse`(`Nume`,`Pret`,`Cantitate`,`Stoc`)VALUES(?,?,?,?);";
	private static String sql = "SELECT `produse`.`idProduse`, `produse`.`Nume`, `produse`.`Pret`,  `produse`.`Cantitate`,`produse`.`Stoc` FROM `baza`.`produse`;";
	private static String updateProd = "UPDATE `baza`.`produse` SET `Nume` = ?, `Pret` = ?, `Cantitate` = ?, `Stoc` = ? WHERE `idProduse` = ?;";
	private static String decr = "UPDATE `baza`.`produse` SET Cantitate = Cantitate - 1 WHERE `idProduse` = ?;";
	private static String selCant = "SELECT `produse`.`Cantitate` FROM `baza`.`produse` ";
	private static String upStoc = "UPDATE `baza`.`produse` SET `Stoc` = ? WHERE `idProduse` = ?;";
	String stk;
	View view;
	
	public static void updateStoc(int id) {
		Connection dbconn = (Connection) ConnectionF.getConnection();
		PreparedStatement upStocSt = null;
		ResultSet rs = null;
		try {
			
			upStocSt = (PreparedStatement) dbconn.prepareStatement(upStoc, Statement.RETURN_GENERATED_KEYS);
			upStocSt.setInt(1, 0);
			upStocSt.setInt(2, id);
			upStocSt.executeUpdate();
			rs = upStocSt.getGeneratedKeys();

		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO: update stoc", e.getMessage());
		} finally { ConnectionF.close(upStocSt);
					ConnectionF.close(dbconn);
		}
	}
	
	public static void getCant(int id) {
		Connection dbconn = (Connection) ConnectionF.getConnection();
		PreparedStatement cantSt = null;
		ResultSet rs = null;
		try {
			cantSt = (PreparedStatement) dbconn.prepareStatement(selCant);
			rs = cantSt.executeQuery(selCant);
			while(rs.next())
			{
				if(rs.getInt("Cantitate")<=0)
					 updateStoc(id);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void UpdateTable() {
		Connection dbconn = (Connection) ConnectionF.getConnection();
		PreparedStatement up = null;
		try {
			String sql = "SELECT * FROM `baza`.`produse`";
			up = (PreparedStatement) dbconn.prepareStatement(sql);
			ResultSet rs = up.executeQuery();
			view.prodTab.setModel(DbUtils.resultSetToTableModel(rs));
			
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO: updateTable", e.getMessage());
		} finally { ConnectionF.close(up);
					ConnectionF.close(dbconn);
		}
	}
	
	public ArrayList<Produs> getAllProd(){
		Connection dbconn = (Connection) ConnectionF.getConnection();
		ArrayList<Produs> prods = new ArrayList<Produs>();
		PreparedStatement findSt = null;
		ResultSet rs = null;
		try {
			findSt = (PreparedStatement) dbconn.prepareStatement(sql);
			rs = findSt.executeQuery(sql);
			while(rs.next())
			{
				if(rs.getInt("Stoc")==1)
					 stk = "In stock";
				else stk = "Out of stock";
				Produs prod = new Produs(rs.getInt("idProduse"), rs.getString("Nume"),rs.getInt("Cantitate"),rs.getInt("Pret"),stk);
				prods.add(prod);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return (prods);
	}
	
	/**
	 * Metoda decrementeaza contitatea produsului, dupa ce acesta a fost achizitionat de catre un client
	 * @param id ID-ul produsului cumparat
	 */
	public static void decrementCant(int id) {
		Connection dbconn = (Connection) ConnectionF.getConnection();
		PreparedStatement decrProd = null;
		ResultSet rs = null;
		int decID = -1;
		try {
			decrProd = (PreparedStatement) dbconn.prepareStatement(decr, Statement.RETURN_GENERATED_KEYS);
			decrProd.setInt(1, id);
			decrProd.executeUpdate();
			rs = decrProd.getGeneratedKeys();
			//System.out.println(x);
			getCant(id);
			if(rs.next()) {
				System.out.println("Decrement success");
			}
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO: Decrement", e.getMessage());
		} finally { ConnectionF.close(decrProd);
					ConnectionF.close(dbconn);
		}
	}
	
	public static int updateProd(Produs produs) {
		Connection dbconnection = (Connection) ConnectionF.getConnection();
		PreparedStatement updateProd2 = null;
		int upId = -1;
		int stoc;
		try {
			
			updateProd2 = (PreparedStatement) dbconnection.prepareStatement(updateProd, Statement.RETURN_GENERATED_KEYS);
			updateProd2.setString(1, produs.getName());
			updateProd2.setInt(2, produs.getPret());
			updateProd2.setInt(3, produs.getCant());
			if(produs.getStoc()=="Out of stock")
				stoc=0;
			else stoc=1;
			updateProd2.setInt(4, stoc);
			updateProd2.setInt(5, produs.getID());
			
			updateProd2.executeUpdate();
			
			ResultSet rs = updateProd2.getGeneratedKeys();
			
			if(rs.next()) {
				System.out.println("sad");
				upId= rs.getInt(1);
			}
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO: update", e.getMessage());
		} finally { ConnectionF.close(updateProd2);
					ConnectionF.close(dbconnection);
		}
		return upId;
	}
	
	public static int insert(Produs produs) {
		Connection dbconnection = (Connection) ConnectionF.getConnection();
		PreparedStatement insertProd2 = null;
		int insertId = -1;
		int stoc;
		try {
			
			insertProd2 = (PreparedStatement) dbconnection.prepareStatement(insertProd, Statement.RETURN_GENERATED_KEYS);
			insertProd2.setString(1, produs.getName());
			insertProd2.setInt(2, produs.getPret());
			insertProd2.setInt(3, produs.getCant());
			if(produs.getStoc()=="Out of stock")
				stoc=0;
			else stoc=1;
			insertProd2.setInt(4, stoc);
			
			insertProd2.executeUpdate();
			
			ResultSet rs = insertProd2.getGeneratedKeys();
			
			if(rs.next()) {

				insertId= rs.getInt(1);
			}
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO: insert", e.getMessage());
		} finally { ConnectionF.close(insertProd2);
					ConnectionF.close(dbconnection);
		}
		return insertId;
	}
}
